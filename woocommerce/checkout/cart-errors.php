<?php
/**
 * Cart errors page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.0
 */


defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>


<p><?php  esc_html_e( 'There are some issues with the items in your cart (shown above). Please go back to the cart page and resolve these issues before checking out.', "rentit" ) ?></p>

<?php do_action( 'woocommerce_cart_has_errors' ); ?>

<p><a class="button wc-backward" href="<?php echo esc_url( wc_get_page_permalink( 'cart' ) ); ?>"><?php  esc_html_e( 'Return To Cart', "rentit" ) ?></a></p>
