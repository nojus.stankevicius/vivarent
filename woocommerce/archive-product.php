<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.5.0
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if (isset($_GET['showas']) && $_GET['showas'] == 'map') {
	get_template_part('template', 'map');
	die();
}

$sidebar_show = true;
if (get_theme_mod('rentit_shop_view_full') == true) {
	$sidebar_show = false;
}

if (isset($_GET['full'])) {
	$sidebar_show = false;
}

get_header('shop');
?>
<div class="content-area">

	<!-- BREADCRUMBS -->

	<!-- /BREADCRUMBS -->

	<!-- PAGE WITH SIDEBAR -->
	<section class="page-section with-sidebar sub-page">
		<div class="container">

			<div class="row">

				<?php
				if (get_theme_mod('rentit_shop_sidebar_pos', 's2') == 's1' && $sidebar_show) {
				?>
					<!-- SIDEBAR -->
					<aside class="col-md-3 sidebar" id="sidebar">
						<?php
						@dynamic_sidebar('rentit_sidebar_shop');
						?>
					</aside>
					<!-- /SIDEBAR -->
				<?php
				}
				?>
				<?php


				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action('woocommerce_before_main_content');
				?>

				<?php if (apply_filters('woocommerce_show_page_title', true)) : ?>

					<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

				<?php endif; ?>

				<?php
				/**
				 * woocommerce_archive_description hook
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				do_action('woocommerce_archive_description');

				?>

				<?php if (wc_get_loop_prop('total')) : ?>

					<?php
					/**
					 * woocommerce_before_shop_loop hook
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */

					do_action('woocommerce_before_shop_loop');
					?>

					<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>

					<?php



					$view_type = get_theme_mod('rentit_shop_view_setting', 'standard');


					if (isset($_GET['view']) && $_GET['view'] == 'grid') {
						$view_type = 'grid';
					} elseif (isset($_GET['view']) && $_GET['view'] == 'standard') {
						$view_type = 'standard';
					} elseif (isset($_GET['view']) && $_GET['view'] == 'grid3') {
						$view_type = 'grid3';
					}



					woocommerce_product_loop_start(); ?>



					<?php while (have_posts()) : the_post();
						if ($view_type == 'standard') {
							wc_get_template_part('content', 'product');
						} else {
							//get_template_part('partials/car', 'list1');

							$class = 'col-md-6';
							if ($view_type == 'grid3') {
								$class = 'col-md-4';
							}



					?>
							<div class="<?php echo esc_attr($class); ?>">
								<?php get_template_part('partials/car', 'list1'); ?>
							</div>
					<?php
						}

					endwhile; // end of the loop. 
					?>

					<?php woocommerce_product_loop_end(); ?>




					<?php woocommerce_product_loop_end(); ?>

					<?php
					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 */

					do_action('woocommerce_after_shop_loop');
					?>

				<?php elseif (!woocommerce_product_subcategories(array(
					'before' => woocommerce_product_loop_start(false),
					'after' => woocommerce_product_loop_end(false)
				))) : ?>

					<?php wc_get_template('loop/no-products-found.php'); ?>

				<?php endif; ?>

				<?php
				/**
				 * woocommerce_after_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action('woocommerce_after_main_content');
				?>

				<?php
				/**
				 * woocommerce_sidebar hook
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */

				?>

				<?php
				if (get_theme_mod('rentit_shop_sidebar_pos', 's2') == 's2' && $sidebar_show) {
				?>
					<!-- SIDEBAR -->
					<aside class="col-md-3 sidebar" id="sidebar">
						<?php
						@dynamic_sidebar('rentit_sidebar_shop');
						?>
					</aside>
					<!-- /SIDEBAR -->
				<?php
				}

				?>


			</div>
		</div>
	</section>
	<!-- /PAGE -->

</div>
<!-- /CONTENT AREA -->


<?php get_footer('shop');
?>