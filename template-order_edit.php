<?php
/**
 * Template Name: order edit
 */


$order_id        = 0;
$last_name       = '';
$error_order_msg = false;

if ( isset( $_GET['order_id'] ) ) {
	$order_id = (int) $_GET['order_id'];

}

if ( isset( $_GET['last_name'] ) ) {
	$last_name = sanitize_text_field( urldecode( $_GET['last_name'] ) );

}
if ( $order_id != 0 ) {

	$post_tupe         = get_post_type( $order_id );
	$billing_last_name = get_post_meta( $order_id, '_billing_last_name', 1 );

	if ( ( $post_tupe != 'shop_order' ) || ( $billing_last_name != $_GET['last_name'] ) ) {
		$order_id = 0;

		$error_order_msg = esc_html__( 'The order number or the last name is incorrect!', 'rentit' );
	}
}

if ( $order_id != 0 ) {
	$order = get_post( $order_id );
	$days  = 0;
	$hour  = 0;

	//var_dump( $order );
	$billing_last_name = get_post_meta( $order_id, '_billing_last_name', 1 );

	//var_dump( get_post_meta( $order_id ) );
	setcookie( 'rentit_order_id', $order_id, time() + 62208000, '/', $_SERVER['HTTP_HOST'] );
	setcookie( 'rentit_billing_last_name', $billing_last_name, time() + 62208000, '/', $_SERVER['HTTP_HOST'] );


	$cd = get_post_meta( 10951, '_carr', 1 );
	//var_dump($cd['custom_data_2']['dropin_location']);
	$cd['custom_data_2']['dropin_location'] = 'ppppkkkk';


	$order      = new WC_Order( $order_id );
	$items      = $order->get_items();
	$product_id = 0;
	//print_r($items);

	$dropoff    = '';
	$dropin     = '';
	$start_date = '';
	$end_date   = '';
	$Day        = '';
	foreach ( $items as $item ) {

		//echo '22222222222222222222'."<br><br><br><br><br>";
		//print_r($item['Dropping Off Date']); echo '<br><br><br>';
		$product_name         = $item['name'];
		$product_id           = $item['product_id'];
		$product_variation_id = $item['variation_id'];
		$dropoff              = $item['Dropping Off Location'];
		$dropin               = $item['Picking Up Location'];
		$start_date           = $item['Picking Up Date'];

		$end_date    = $item['Dropping Off Date'];
		$my_end_date = $item['Dropping Off Date'];
		if ( isset( $item['Day(s)'] ) ) {
			$Day = $item['Day(s)'];
		}

	}

	/*if ( isset( $_GET['addcar_id']{1} ) ) {
		$product_id = (int) $_GET['addcar_id'];
	}*/

	setcookie( 'rentit_order_start_date', $start_date, time() + 62208000, '/', $_SERVER['HTTP_HOST'] );
	setcookie( 'rentit_order_end_date', $end_date, time() + 62208000, '/', $_SERVER['HTTP_HOST'] );
	setcookie( 'rentit_order_dropin', $dropin, time() + 62208000, '/', $_SERVER['HTTP_HOST'] );
	setcookie( 'rentit_order_dropoff', $dropoff, time() + 62208000, '/', $_SERVER['HTTP_HOST'] );


	$new_product_id = '';
	if ( isset( $_GET['addcar_id'] ) ) {
		$new_product_id = $_GET['addcar_id'];
	} else {
		$new_price = rentit_get_current_price_product( $product_id );

	}


	if ( isset( $_GET['dropin'] ) ) {
		$dropin = urldecode( $_GET['dropin'] );
	}

	if ( isset( $_GET['dropoff'] ) ) {
		$dropoff = urldecode( $_GET['dropoff'] );
	}

	if ( isset( $_GET['start_date'] ) ) {
		$start_date = urldecode( $_GET['start_date'] );
	}

	if ( isset( $_GET['end_date'] ) ) {
		$end_date = urldecode( $_GET['end_date'] );
	}


	if ( isset( $_GET['start_date'] ) && $_GET['end_date'] ) {
		$Day  = urldecode( $_GET['end_date'] );
		$Day  = rentit_DateDiff( 'd', strtotime( $_GET['start_date'] ), strtotime( $_GET['end_date'] ) );
		$days = $Day;
		$hour = rentit_DateDiff( 'h', strtotime( $_GET['start_date'] ), strtotime( $_GET['end_date'] ) );
		if ( $Day < 1 ) {
			$Day = 1;
		}


	} else {
		$Day  = rentit_DateDiff( 'd', strtotime( $start_date ), strtotime( $end_date ) );
		$days = $Day;
		$hour = rentit_DateDiff( 'h', strtotime( $start_date ), strtotime( $end_date ) );
	}


	$ecount            = wc_update_order_item_meta( $order_id, 'end_count', $cd );
	$rental_resources2 = array();
	$rental_resources  = get_post_meta( $product_id, '_rental_resources', 1 );
	if ( isset( $_GET['addcar_id']{1} ) ) {
		$rental_resources2 = get_post_meta( (int) $_GET['addcar_id'], '_rental_resources', 1 );
	}

	$arr_extrax = array();

	if ( $rental_resources ) {
		foreach ( $rental_resources as $item ) {
			$arr_extrax[] = $item['item_name'];
		}
	}

	if ( $rental_resources2 ) {
		foreach ( $rental_resources2 as $item ) {
			$arr_extrax[] = $item['item_name'];
		}
	}
	//var_dump( $_GET );
	//update_post_meta( 10951, '_carr', $cd )


	//esxtrax
	$arr_extrax_k_v = array();
	foreach ( $items as $item ) {

		$extra_c_arrr = array();

		foreach ( $item as $key => $v ) {
			if ( is_array( $v ) ) {
				continue;
			}
			//	echo $key;
			if ( ! in_array( $key, $arr_extrax ) ) {
				continue;
			}
			$arr_extrax_k_v[ $key ] = $v;

		}

	}
	setcookie( 'rentit_order_dropoff', json_encode( $arr_extrax_k_v ), time() + 62208000, '/', $_SERVER['HTTP_HOST'] );


	$eroor_msg = false;
	//var_dump($_POST);
	if ( isset( $_POST['checkout'] ) ) {


		$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $new_product_id ) );
		$quantity          = 1;
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		$product_status    = get_post_status( $product_id );


		// cleaer car before add
		global $woocommerce;


		$woocommerce->cart->empty_cart();
		WC()->cart->empty_cart();
		//do_action( 'woocommerce_cart_emptied' );

		if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {

			do_action( 'woocommerce_ajax_added_to_cart', $product_id );

			if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
				wc_add_to_cart_message( $product_id );
			}



			global $woocommerce;
			$checkout_url = wc_get_checkout_url();

			/*
			 * set cansel status to priev order
			 *
			 */
			/*$order             = get_post( (int) $_GET['order_id'] );
			$billing_last_name = get_post_meta( (int) $_GET['order_id'], '_billing_last_name', 1 );

			if ( $order->post_type == 'shop_order' && $billing_last_name == urldecode( $_POST['last_name'] ) ) {
				wp_transition_post_status( 'wc-cancel-request', get_post_status( (int) $_POST['order_id'] ),
					$order );


				$my_post = array(
					'ID'          => (int) $_POST['order_id'],
					'post_status' => 'wc-cancel-request'
				);


				$post = wp_update_post( $my_post );
				if ( ! is_wp_error( $post ) ) {
					wp_die( 1 );
				} else {
					echo $post->get_error_message();
				}

			} else {
				echo esc_html__( 'something wrong try again', 'rentit' );
			}
*/

			/*
			 *
			 */
			//var_dump($checkout_url);
			wp_safe_redirect( $checkout_url );
			//var_dump($_POST)	;	echo 'ok';
			// Return fragments
			//self::get_refreshed_fragments();

		} else {
			//var_dump($_POST)	;

			$eroor_msg = true;

		}

	}
	$extra_price = 0;

	/*
	 *
	 */
	ob_start();
	?>
	<div class="col-md-12">
		<br><br>
		<h3 class="block-title alt"><i class="fa fa-angle-down"></i>
			RENTAL OPTION
		</h3>
		<table class="shop_table order_details table table-striped">
			<thead>
			<tr>
				<th class="order-number"><span class="nobr">Extra</span></th>
				<th class="order-date"><span class="nobr">Total</span></th>

			</tr>
			</thead>
			<tbody>
			<?php
			$arr_selected_extarax = array();
			$rental_resurse       = get_post_meta( $product_id, '_rental_resources', 1 );
			$arr_selected_Extra   = array();
			//$name_extra_arr = array();

			//	var_dump($rental_resurse[0]);

			foreach ( $items as $item ) {


				$extra_c_arrr = array();
				//var_dump( $arr_extrax );
				foreach ( $item as $key => $v ) {
					if ( is_array( $v ) ) {
						continue;
					}
					//	echo $key;
					if ( ! in_array( $key, $arr_extrax ) ) {
						continue;
					}
					$arr_selected_extarax[] = $key;
					?>
					<tr class="order">
						<td class="order-number" data-title="Order">
							<?php echo $key; ?>
						</td>
						<td class="order-date" data-title="Date">
							<?php echo $v; ?>
						</td>

					</tr>
					<?php
				}

			}
			// var_dump($arr_selected_extarax);
			if ( $rental_resurse ) {
				foreach ( $rental_resurse as $res ) {

					//var_dump($res);
					if ( in_array( $res['item_name'], $arr_selected_extarax ) ) {

						//$price
						if ( $res["duration_type"] == "days" ) {
							$extra_price += ( $res["cost"] * $days );
							//	var_dump($res);

						}
						if ( $res["duration_type"] == "hours" ) {
							$extra_price += ( $res["cost"] * $hour );
						}
						if ( $res["duration_type"] == "total" ) {
							$extra_price += $res["cost"];
						}
					}

				}
			}

			?>

			</tbody>
		</table>
		<?php

		$my_product_id = $product_id;
		if ( isset( $_GET['addcar_id']{1} ) ) {
			$my_product_id = (int) $_GET['addcar_id'];
		}

		/**
		 * extras
		 */
		$arr_resources = ( get_post_meta( $my_product_id, '_rental_resources', true ) );
		//var_dump($arr_resources);
		$j = 0;
		if ( $arr_resources ) {
			foreach ( $arr_resources as $item ) {
				//var_dump( $arr_resource );
				//echo $item['item_name'];

				?>

				<input style="display: none;" name="checkbox_extras[<?php echo esc_attr( $j ); ?>]" data-price="12"
				       type="checkbox" <?php if ( in_array( $item['item_name'], $arr_selected_extarax ) ) {
					echo 'checked=""';
				} ?>>

				<?php
				$j ++;
			}
		}
		/**
		 *
		 * extras
		 */
		?>
	</div><?php
	$rental_extarx = ob_get_clean();

	/*
	 *
	 */

	//$price = $order->get_total();

	//if ( isset( $_GET['addcar_id'] ) ) {

	//departure date

	$new_price      = array();
	$new_product_id = '';
	if ( isset( $_GET['addcar_id'] ) ) {
		$new_price      = rentit_get_current_price_product( (int) $_GET['addcar_id'] );
		$new_product_id = $_GET['addcar_id'];
	} else {
		$new_price      = rentit_get_current_price_product( $product_id );
		$new_product_id = $product_id;
	}


	//dates
	$days = rentit_DateDiff( 'd', strtotime( $start_date ), strtotime( $end_date ) );
	$hour = rentit_DateDiff( 'h', strtotime( $start_date ), strtotime( $end_date ) );

	if ( $days < 1 ) {
		$days = 1;
	}


	$new_price = $new_price * $days;
	$resources = get_post_meta( $new_product_id, '_rental_discounts', true );


	if ( $resources ) {
		$arr_hour = array();
		$arr_day  = array();
		foreach ( $resources as $key => $discounts ) {
			// var_dump($discounts);
			if ( $discounts['duration_type'] == 'days' ) {
				$arr_day[ $discounts['duration_val'] ] = array(
					'cost' => $discounts['cost'],

				);
			}
			if ( $discounts['duration_type'] == 'hours' ) {
				$arr_hour[ $discounts['duration_val'] ] = array(
					'cost' => $discounts['cost'],

				);
			}


		}


		// short to heaght deay
		krsort( $arr_day );
		krsort( $arr_hour );

		foreach ( $arr_day as $key => $price_disc ) {
			if ( $days >= $key ) {
				$new_price = $price_disc['cost'] * $days;
				break;
			}

		}

		if ( $arr_hour && $hour < 24 ) {

			///determine the largest number to the specified
			foreach ( $arr_hour as $key => $price_disc ) {
				if ( $hour >= $key ) {
					$new_price = $price_disc['cost'] * $hour;
					break;
				}

			}
		}

	}
	//var_dump($new_price);

	//}

	//var_dump($extra_price);
	//$price = $price = + $extra_price;
	//var_dump( $price );
	//var_dump( get_post_meta( $product_id, '_rental_resources', true ) );
	get_header();

	?>

        });

    <?php
	if ( $eroor_msg ) {

		?>
		<div class="content-area mycontent_area"><br><br>
			<div class="container">
				<?php wc_print_notices(); ?>
			</div>
		</div>
		<?php
	}


	?>
	<div class="content-area mycontent_area">
		<div class="container">
			<br>
			<div class="col-md-12">
				<br>
				<h3 class="block-title alt"><i class="fa fa-angle-down"></i>
					reservation code <?php echo esc_html( $order_id ); ?> edit
				</h3>

				<?php
				the_post();
				the_content();
				?>
			</div>
			<form action="#" method="post">
				<div class="col-md-4">
					<h3 class="block-title alt"><i class="fa fa-angle-down"></i>
						<?php esc_html_e( 'period', 'rentit' ) ?>
					</h3>
					<div class="media">
						<span class="media-object pull-left"><i class="fa fa-calendar"></i></span>
						<div class="media-body">


							<?php
							//new car
							/*	if ( isset( $_GET['addcar_id']{1} ) ) {
									$product_id = (int) $_GET['addcar_id'];
								} else {*/
							echo esc_html( $start_date );
							//	}


							?>
							<input type="hidden" name="dropin_date" value="<?php echo esc_html( $start_date ); ?>">

						</div>

					</div>
					<div class="media">
						<span class="media-object pull-left"><i class="fa fa-calendar"></i></span>
						<div class="media-body">
							<?php
							/*if ( isset( $_GET['addcar_id']{1} ) ) {
								$product_id = (int) $_GET['addcar_id'];
							} else {*/
							echo esc_html( $end_date );
							//}


							?>
							<input type="hidden" name="dropoff_date" value="<?php echo esc_html( $end_date ); ?>">

						</div>

					</div>
					<div class="media">
						<span class="media-object pull-left"><i class="fa fa-clock-o"></i></span>
						<div class="media-body">
							<b><?php esc_html_e( 'Durations', 'rentit' ); ?></b> <?php echo esc_html( $Day ); ?> <?php esc_html_e( 'days', 'rentit' ); ?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<h3 class="block-title alt"><i
							class="fa fa-angle-down"></i><?php esc_html_e( 'pickup', 'rentit' ) ?></h3>
					<div class="media">
						<span class="media-object pull-left"><i class="fa fa-calendar"></i></span>
						<div class="media-body">
							<?php
							/*if ( isset( $_GET['addcar_id']{1} ) ) {
								$product_id = (int) $_GET['addcar_id'];
							} else {*/
							echo esc_html( $dropin );
							//}
							?>
							<input type="hidden" name="dropin_location" value="<?php echo esc_html( $dropin ); ?>">
						</div>
					</div>


				</div>
				<div class="col-md-4">
					<h3 class="block-title alt"><i
							class="fa fa-angle-down"></i><?php esc_html_e( 'return', 'rentit' ) ?></h3>
					<div class="media">
						<span class="media-object pull-left"><i class="fa fa-calendar"></i></span>
						<div class="media-body">
							<?php
							/*if ( isset( $_GET['addcar_id']{1} ) ) {
								$product_id = (int) $_GET['addcar_id'];
							} else {*/
							echo esc_html( $dropoff );
							//}
							?>

							<input type="hidden" name="dropoff_location" value="<?php echo esc_html( $dropoff ); ?>">
						</div>
					</div>

				</div>
				<div class="col-md-12">
					<br><br>
					<h3 class="block-title alt"><i class="fa fa-angle-down"></i>
						SELECTED CARS
					</h3>
					<table class="shop_table order_details table">
						<thead>
						<tr>
							<th class="order-number"><span class="nobr">Car</span></th>
							<th class="order-date"><span class="nobr">Total</span></th>
						</tr>
						</thead>
						<tbody>
						<tr class="order">
							<td class="order-number" data-title="Order">
								<?php
								if ( isset( $_GET['addcar_id'] ) ) {
									echo esc_html( get_the_title( (int) $_GET['addcar_id'] ) );
								} else {
									echo esc_html( get_the_title( $product_id ) );
								}

								?>
							</td>
							<td class="order-date" data-title="Date">
								<?php
								/*if ( isset( $_GET['addcar_id']{1} ) ) {
									$product_id = (int) $_GET['addcar_id'];
								} else {*/
								echo $new_price + $extra_price;
								//}
								?>
								$
							</td>

						</tr>
						</tbody>
					</table>
				</div>
				<!-------------------------------------------------------------->

				<?php echo $rental_extarx; ?>
				<!---------------------------------------------------------------------->

				<?php ?>
				<input type="hidden" name="add-to-cart" value="<?php if ( isset( $_GET['addcar_id']{1} ) ) {
					echo esc_attr( $_GET['addcar_id'] );
				} else {
					echo esc_attr( $product_id );
				} ?>">
				<?php
				$arr_url = array(
					'dropin'     => $dropin,
					'dropoff'    => $dropoff,
					'start_date' => $start_date,
					'end_date'   => $end_date,
					'edit_car'   => 1


				);


				?>
				<div class="col-md-12">
					<div class="pull-right">
						<a data-toggle="modal" data-target="#myModal" class="btn btn-theme"
						   href="#">
							Cancel Reservation</a>
						<a class="btn btn-theme" onclick="setcookie"
						   href="<?php echo esc_url( get_permalink( wc_get_page_id( ( 'shop' ) ) ) . '?' . http_build_query( $arr_url ) ); ?>">
							Change Car
						</a>
						<a data-toggle="modal" data-target="#myModal_change_time" class="btn btn-theme"
						   href="#">
							Change Date & Time
						</a>
						<a data-toggle="modal" data-target="#myModal_change_user_details" class="btn btn-theme"
						   href="#">
							Update Details
						</a>
						<button name="checkout" type="submit" class="btn btn-theme"
						>
							Checkout
						</button>
					</div>
				</div>
			</form>

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Cancel Reservation</h4>
						</div>
						<div class="modal-body">
							<?php esc_html_e( 'You seriously want to cancel a reservation?', 'rentit' ); ?>
						</div>
						<div class="modal-footer">
							<img class="ajax-loader_img"
							     src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/25.gif">

							<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
							<button type="button" class="btn btn-primary  cansel-reserve">Yes</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="myModal_change_time" tabindex="-1" role="dialog"
			     aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Change Date & Time</h4>
						</div>
						<div class="modal-body">
							<div class="form-search light">
								<form class="order_cange_date_time" action="<?php
								if ( function_exists( 'wc_get_page_id' ) ) {
									echo esc_url( get_permalink( wc_get_page_id( ( 'shop' ) ) ) );
								}

								?>">


									<div class="row row-inputs">
										<div class="container-fluid">
											<div class="col-sm-12">
												<div class="form-group has-icon has-label">

													<label
														for="formSearchUpLocation3"><?php esc_html_e( 'Picking Up Location', 'rentit' ) ?> </label>
													<input type="text" class="form-control formSearchUpLocation2"
													       id="formSearchUpLocation3"
													       name="dropin"
													       placeholder="<?php esc_html_e( 'Airport or Anywhere', 'rentit' ); ?>"
													       value="<?php /*
													       if ( function_exists( 'rentit_get_date_s' ) ) {
														       rentit_get_date_s( 'dropin_location' );
													       }*/
													       ?>"
													>
													<?php

													// get Picking Up Location

													if ( isset( $_GET['addcar_id']{1} ) ) {

														$pick_up_location = get_post_meta( $_GET['addcar_id'], '_rental_dropin_locations2', true );
													} else {

														$pick_up_location = get_post_meta( $product_id, '_rental_dropin_locations2', true );
													}


													$locaton_arr = array();
													$locaton_str = '';
													if ( $pick_up_location ) {
														foreach ( $pick_up_location as $location ) {
															$locaton_arr[] = "'" . $location . "'";
														}
														$locaton_str = implode( ',', $locaton_arr );
													}


													?>
													<script>
														jQuery(document).ready(function ($) {
															$('.formSearchUpLocation2').typeahead({
																minLength: 0,
																source: [<?php echo wp_kses_post( $locaton_str ); ?>]
															});
														});
													</script>
													<?php unset( $locaton_arr, $locaton_str, $pick_up_location ); ?>


													<span class="form-control-icon"><i
															class="fa fa-map-marker"></i></span>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group has-icon has-label">


													<label
														for="formSearchOffLocation3"><?php esc_html_e( 'Dropping Off Location', 'rentit' ) ?></label>

													<input name="dropoff" type="text"
													       class="form-control formSearchUpLocation20"
													       id="formSearchOffLocation3"
													       placeholder="<?php esc_html_e( 'Airport or Anywhere', 'rentit' ); ?>"
													       value="<?php
													       /*
																													  if ( function_exists( 'rentit_get_date_s' ) ) {
																														  rentit_get_date_s( 'dropoff_location' );
																													  }*/
													       ?>">
													<?php

													// get PDropping Off Location


													if ( isset( $_GET['addcar_id']{1} ) ) {

														$pick_up_location = get_post_meta( $_GET['addcar_id'], '_rental_dropoff_locations2', true );
													} else {

														$pick_up_location = get_post_meta( $product_id, '_rental_dropoff_locations2', true );
													}


													$locaton_arr = array();
													$locaton_str = '';
													if ( $pick_up_location ) {
														foreach ( $pick_up_location as $location ) {
															$locaton_arr[] = "'" . $location . "'";
														}
														$locaton_str = implode( ',', $locaton_arr );

													}
													?>
													<script>
														jQuery(document).ready(function ($) {
															$('.formSearchUpLocation20').typeahead({
																minLength: 0,
																source: [<?php echo wp_kses_post( $locaton_str ); ?>]
															});
														});
													</script>
													<?php unset( $locaton_arr, $locaton_str, $pick_up_location ); ?>


													<span class="form-control-icon"><i
															class="fa fa-map-marker"></i></span>
												</div>
											</div>
										</div>
									</div>

									<div class="row row-inputs">
										<div class="container-fluid">
											<div class="col-sm-12">
												<div class="form-group has-icon has-label">
													<label
														for="formSearchUpDate50">    <?php esc_html_e( ' Picking Up Date', 'rentit' ); ?></label>
													<input name="start_date" type="text" class="form-control"
													       id="formSearchUpDate50"
													       value="<?php
													       /*  if ( function_exists( 'rentit_get_date_s' ) ) {
																 rentit_get_date_s( 'dropin_date' );
															 }*/
													       echo $start_date;
													       ?>"
													       placeholder="<?php esc_html_e( 'dd/mm/yyyy', 'rentit' ); ?>">
                                                                    <span class="form-control-icon"><i
		                                                                    class="fa fa-calendar"></i></span>
												</div>
											</div>

										</div>
									</div>

									<div class="row row-inputs">
										<div class="container-fluid">
											<div class="col-sm-12">
												<div class="form-group has-icon has-label">
													<label for="formSearchUpDate60">
														<?php esc_html_e( 'Dropping Off Date', 'rentit' ); ?></label>
													<input name="end_date" type="text" class="form-control"
													       id="formSearchUpDate60"
													       value="<?php
													       /*if ( function_exists( 'rentit_get_date_s' ) ) {
														       rentit_get_date_s( 'dropoff_date' );
													       }*/
													       echo $end_date;
													       ?>"
													       placeholder="<?php esc_html_e( 'dd/mm/yyyy', 'rentit' ); ?>">
                                                                    <span class="form-control-icon"><i
		                                                                    class="fa fa-calendar"></i></span>
												</div>
											</div>

										</div>
									</div>

									<div class="row row-inputs">
										<div class="container-fluid">
											<div class="col-sm-12  date_time_ajax_message"></div>
										</div>
									</div>

									<div class="row row-submit">
										<div class="container-fluid ">
											<div>
												<img class="ajax-loader_img"
												     src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/25.gif">

												<button type="submit" id="rentit_updatedate"
												        class="btn btn-submit btn-theme ripple-effect pull-right">
													<?php esc_html_e( 'Done', 'rentit' ); ?>
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>

			<!-- Modal 2 -->

			<div class="modal fade" id="myModal_change_user_details" tabindex="-1" role="dialog"
			     aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
									aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Change Details </h4>
						</div>
						<div class="modal-body ">
							<div class="form-search light">
								<form action="#" method="POST" role="form" class="form-extras cha">

									<div class="">


										<div class="col-md-6">
											<div class="left">
												<?php $arr_resources = ( get_post_meta( $my_product_id, '_rental_resources', true ) );

												if ( ! empty( $arr_resources ) ):
													foreach ( $arr_resources as $item ) {
														$type = '';

														// for translation
														if ( $item["duration_type"] == 'days' ) {
															$type = esc_html__( 'Days', 'rentit' );
														}
														if ( $item["duration_type"] == 'hours' ) {
															$type = esc_html__( 'Hours', 'rentit' );
														}
														if ( $item["duration_type"] == 'total' ) {
															$type = esc_html__( 'Total', 'rentit' );
														}
														if ( $item["duration_type"] == 'Included' ) {
															$type = esc_html__( 'Included', 'rentit' );
														}


														$val     = $item["cost"] . " " . get_woocommerce_currency_symbol( get_option( 'woocommerce_currency' ) ) . ' / ' . $type;
														$checked = false;
														$disable = false;
														if ( $item["cost"] == '0' || empty( $item["cost"] ) ) {
															$val     = esc_html__( 'Free', 'rentit' );
															$checked = true;

														}
														if ( $item["duration_type"] == 'total' ) {

															$val     = $item["cost"] . " " . get_woocommerce_currency_symbol( get_option( 'woocommerce_currency' ) ) . ' / ' . $type;
															$checked = false;
														}
														if ( $item["duration_type"] == 'Included' ) {
															$val     = $type;
															$checked = true;
															$disable = true;

														}


														$arr_extras[] = array(
															'value'   => $val,
															'name'    => esc_html( $item["item_name"] ),
															'price'   => esc_attr( $item["cost"] ),
															'checked' => $checked,
															'disable' => $disable

														);

													}
												endif;

												?>
												<?php

												if ( ! empty( $arr_resources ) ):
													$how = ceil( count( $arr_extras ) / 2 );
													$i = 0;
													//var_dump($arr_selected_extarax);
													while ( $i < $how ) {

														?>
														<div class="checkbox checkbox-danger">
															<input name="checkbox_extras[<?php echo esc_attr( $i ); ?>]"
															       data-price="<?php echo @esc_attr( $arr_extras[ $i ]['price'] ); ?>"
															       id="checkboxl<?php echo esc_attr( $i ); ?>"
															       type="checkbox"
																<?php if ( in_array( $arr_extras[ $i ]['name'], $arr_selected_extarax ) ) {
																	echo 'checked=""';
																} ?>

																<?php if ( $arr_extras[ $i ]['disable'] == true && $arr_extras[ $i ]['checked'] == true ) { ?>
																	checked=""
																	disabled="disabled"
																<?php } ?>
															>

															<label
																for="checkboxl<?php echo esc_attr( $i ); ?>"><?php echo @esc_attr( $arr_extras[ $i ]['name'] ); ?>
																<span
																	class="pull-right">
                                            <?php echo @esc_attr( $arr_extras[ $i ]['value'] ); ?> </span></label>
														</div>
														<?php
														$i ++;
													}
												endif;

												?>

											</div>
										</div>


										<div class="col-md-6">
											<div class="right">
												<?php
												if ( ! empty( $arr_resources ) ):
													while ( $i < count( $arr_extras ) ) {

														?>
														<div class="checkbox checkbox-danger">
															<input name="checkbox_extras[<?php echo esc_attr( $i ); ?>]"
															       data-price="<?php echo @esc_attr( $arr_extras[ $i ]['price'] ); ?>"
															       id="checkboxl<?php echo esc_attr( $i ); ?>"
															       type="checkbox"

																<?php if ( in_array( $arr_extras[ $i ]['name'], $arr_selected_extarax ) ) {
																	echo 'checked=""';
																} ?>

																<?php if ( $arr_extras[ $i ]['disable'] == true && $arr_extras[ $i ]['checked'] == true ) { ?>
																	checked=""
																	disabled="disabled"
																<?php } ?>


															>
															<label
																for="checkboxl<?php echo esc_attr( $i ); ?>"><?php echo @esc_attr( $arr_extras[ $i ]['name'] ); ?>
																<span
																	class="pull-right"><?php echo @esc_attr( $arr_extras[ $i ]['value'] ); ?> </span></label>
														</div>
														<?php
														$i ++;

													}
												endif;
												?>


											</div>
										</div>

									</div>
									<div class="container-fluid ">
										<div>
											<img class="ajax-loader_img"
											     src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/25.gif">

											<button type="submit" id="rentit_update_extra"
											        class="btn btn-submit btn-theme ripple-effect pull-right">
												Done
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>


		</div>


	</div>
	<script>
		jQuery(document).ready(function ($) {
			//$('.js-fbl').addClass('btn btn-theme btn-block btn-icon-left facebook');
			//$('.js-fbl').html('   <i class="fa fa-facebook"></i> <?php   esc_html_e( 'Sign in with Facebook', 'rentit' ); ?>');
			//	jQuery('.form-login').serialize();
			$('#rentit_updatedate').click(function (e) {
				e.preventDefault();// + jQuery('.order_cange_date_time').serialize()
				$(this).prop('disabled', true);

				jQuery('.ajax-loader_img').show();
				var mainthis = $(this);
				jQuery.ajax({
					url: rentit_obj.ajaxurl,
					type: 'POST',
					data: 'action=rentit_set_order_time_date&order_id=<?php echo esc_html( $order_id ); ?>&' + jQuery('.order_cange_date_time').serialize(),
					success: function (html) {
						jQuery('.ajax-loader_img').hide();
						mainthis.parent().append(html);
						mainthis.prop('disabled', false);
						setTimeout(function () {
							location.reload();
						}, 1000);
					}
				});

			});

			$('.cansel-reserve').click(function (e) {

				e.preventDefault();
				jQuery('.ajax-loader_img').show();
				jQuery.ajax({
					url: rentit_obj.ajaxurl,
					type: 'POST',
					data: 'action=rentit_set_order_status&order_id=<?php echo esc_html( $order_id ); ?>&last_name=<?php echo esc_html( $billing_last_name ); ?>',
					success: function (html) {
						jQuery('.ajax-loader_img').hide();
						if (html == "1") {
							$('#myModal  .modal-body').html('<?php  esc_html_e( 'Thank you cancel the order' ) ?> <br> <?php esc_html_e( 'for a few seconds going a redirect to the home page', 'rentit' ); ?>');

							setTimeout(function () {
								window.location = '<?php echo esc_html( get_home_url( '/' ) ); ?>';
							}, 1000);

						} else {

							$('#myModal  .modal-body').append(html);
						}
						console.log(html);
					}
				});
			});


			//rentit_update_extra
			$('#rentit_update_extra').click(function (e) {
				e.preventDefault();
				//alert(3);
				jQuery('.ajax-loader_img').show();
				jQuery.ajax({
					url: rentit_obj.ajaxurl,
					type: 'POST',
					data: 'action=rentit_set_order_extras&addcarid=<?php if ( isset( $_GET['addcar_id'] ) ) {
						echo sanitize_text_field( $_GET['addcar_id'] );
					} ?>&product_id=<?php echo esc_html( $product_id ); ?>&order_id=<?php echo esc_html( $order_id ); ?>&last_name=<?php echo esc_html( $billing_last_name ); ?>&' + $('.form-extras').serialize(),
					success: function (html) {
						jQuery('.ajax-loader_img').hide();
						setTimeout(function () {
							location.reload();
						}, 500);


						console.log(html);
					}
				});
			});


		});
	</script>
	<?php
} else {
	get_header();
	?>
	<div class="content-area">
		<!-- BREADCRUMBS -->
		<!-- BREADCRUMBS -->
		<?php
		$brdc               = get_post_meta( $post->ID, '_rentit_breadcrumbs_aling', true );
		$rentit_breadcrumbs = ! empty( $brdc ) ? $brdc : "right";
		?>
		<section class="page-section breadcrumbs text-<?php echo esc_attr( $rentit_breadcrumbs ); ?>">
			<div class="container">

			</div>
		</section>
		<!-- /BREADCRUMBS -->
		<!-- /BREADCRUMBS -->
		<!-- PAGE WITH SIDEBAR -->
		<section class="page-section with-sidebar">
			<div class="container" style="min-height: 50vh;">
				<div class="row">

					<div class="col-md-12 content" id="content">
						<div class="alert alert-danger">
							<strong>	<?php echo esc_html( $error_order_msg ); ?></strong>

						</div>

						<!-- /PAGE -->
					</div>
					<!-- CONTENT -->

				</div>
			</div>
		</section>
		<!-- /PAGE WITH SIDEBAR -->
	</div>
	<?php
}
get_footer();
?>