<!-- FOOTER -->
<?php
$val           = wp_kses_post(get_post_meta(@$post->ID, '_rentit_shortcode_footer', true));
$rentit_header = wp_kses_post(get_post_meta(@$post->ID, 'rentit_hide_footer', true));
$is_shop       = false;
// contact form in shop
if (function_exists('is_shop') && is_shop()) {
	$is_shop   = true;
	$shortcode = wp_kses_post(get_theme_mod('rentit_c_form_s_val'));
	if (isset($shortcode) && !empty($shortcode)) {
		echo do_shortcode($shortcode);
	}
}
$hide_footer = get_post_meta(get_the_ID(), 'rentit_hide_footer', false);
//var_dump(get_post_meta());
if (is_front_page() || $rentit_header == true || $is_shop) { ?>
	<?php if (isset($val) && !empty($val)) :
		echo do_shortcode(wp_kses_post($val));
	endif; ?>

	<footer class="footer">
		<div class="footer-meta">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<a href="/privacy-policy" class="" target="_blank">Privatumo politika</a>
						<a href="/privacy-policy" class="" target="_blank">Slapukai </a>
						<a href="/privacy-policy" class="" target="_blank">Ilgalaikė nuoma </a>
						<a href="/privacy-policy" class="" target="_blank">Nuomos sąlygos</a>
					</div>
					<div class="col-sm-12">
						<p class="btn-row text-center">
							<?php if (strlen(get_theme_mod('sotial_networks_control_facebook')) > 8) : ?>
								<a target="_blank" href="https://www.facebook.com/vivarent.lt" class="btn btn-theme btn-icon-left facebook"><i class="fa fa-facebook"></i>
									<?php esc_html_e('FACEBOOK', 'rentit'); ?>
								</a>
							<?php endif; ?>

						</p>
						<div class="copyright">
							&copy; <?php echo date("Y"); ?>
							<?php esc_html_e('  Vivarent - Visos teisės saugomos', 'rentit'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
<?php } else { ?>
	<!-- FOOTER -->
	<?php if (isset($val) && !empty($val)) :
		echo do_shortcode(wp_kses_post($val));
	endif; ?>
	<footer class="footer">
		<div class="footer-meta">
			<div class="container">
				<div class="row">

					<div class="col-sm-12">
						<p class="btn-row text-center">
							<?php if (strlen(get_theme_mod('sotial_networks_control_facebook')) > 8) : ?>
								<a target="_blank" href="https://www.facebook.com/vivarent.lt" class="btn btn-theme btn-icon-left facebook"><i class="fa fa-facebook"></i>
									<?php esc_html_e('FACEBOOK', 'rentit'); ?>
								</a>
							<?php endif; ?>

						</p>
						<div class="copyright">
							&copy; <?php echo date("Y"); ?>
							<?php esc_html_e('  Vivarent - Visos teisės saugomos', 'rentit'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /FOOTER -->
<?php } ?>
<!-- /FOOTER -->
<div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>
</div>
<!-- /WRAPPER -->

<script>
	jQuery(document).ready(function($) {

		$('#tabs1 li a').click(function() {
			jQuery(window).trigger('resize');
		});
	});
</script>
<?php wp_footer(); ?>
</body>

</html>