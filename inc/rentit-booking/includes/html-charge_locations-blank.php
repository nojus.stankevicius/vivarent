<?php
global $j;
$charge_location = null;
if ( !$j ) {
	$j = 0;
}


?>

<tr class="tbody_charge_locations_tr">

    <td class="cost" width="11%">
		<?php

		$locations_saved = $charge_location['drop-in'];

		?>


        <select multiple="multiple" class="multiselect wc-enhanced-select drop-in"
                name="_rental_charge_locations[<?php echo esc_attr( $j ); ?>][drop-in][]" style="width: 90%;"
                data-placeholder="<?php echo esc_html__( 'Select drop-in location', 'rentit' ); ?>">

			<?php

			$arr_location = array();
			$args = array(
				'showposts' => 10000,
				'post_status' => 'publish',
				'post_type' => 'rental_location',
				'orderby' => 'title',
				'order' => 'ASC'

			);
			unset( $terms );
			$rentit_custom_query = new  WP_Query( $args );
			if ( $rentit_custom_query->have_posts() ):
				while ( $rentit_custom_query->have_posts() ):
					$rentit_custom_query->the_post();
					if ( is_array( $locations_saved ) ) {

						echo '<option value="' . esc_html( get_the_title() ) . '" ' . selected( in_array( get_the_title(), $locations_saved ), true ) . '>' .
						     esc_html( get_the_title() ) . '</option>';
					} else {
						echo '<option value="' . esc_html( get_the_title() ) . '" ' . '>' .
						     esc_html( get_the_title() ) . '</option>';
					}
				endwhile;

				wp_reset_postdata();
			else:
				echo '<option value="">' . esc_html__( 'No locations found.', 'rentit' ) . '</option>';
			endif;


			?>

        </select>

    </td>
    <td class="duration" width="15%">

		<?php
		$locations_saved_2 = isset( $charge_location['drop-off'] ) ? $charge_location['drop-off'] : ''


		?>


        <select multiple="multiple" class="multiselect wc-enhanced-select drop-off"
                name="_rental_charge_locations[<?php echo esc_attr( $j ); ?>][drop-off][]" style="width: 90%;"
                data-placeholder="<?php echo esc_html__( 'Select drop-in location', 'rentit' ); ?>">

			<?php

			$args = array(
				'showposts' => 10000,
				'post_status' => 'publish',
				'post_type' => 'rental_location',
				'orderby' => 'title',
				'order' => 'ASC'

			);
			unset( $terms );
			$rentit_custom_query = new  WP_Query( $args );
			if ( $rentit_custom_query->have_posts() ):
				while ( $rentit_custom_query->have_posts() ):
					$rentit_custom_query->the_post();
					if ( is_array( $locations_saved ) ) {

						echo '<option value="' . esc_html( get_the_title() ) . '" ' . selected( in_array( get_the_title(), $locations_saved ), true ) . '>' .
						     esc_html( get_the_title() ) . '</option>';
					} else {
						echo '<option value="' . esc_html( get_the_title() ) . '" ' . '>' .
						     esc_html( get_the_title() ) . '</option>';
					}
				endwhile;

				wp_reset_postdata();
			else:
				echo '<option value="">' . esc_html__( 'No locations found.', 'rentit' ) . '</option>';
			endif;


			?>
        </select>

    </td>
    <td class="duration" width="15%">
        <input type="text" class="input_text days"
               placeholder="days" name="_rental_charge_locations[<?php echo esc_attr( $j ); ?>][days][]" value="<?php
		if ( isset( $charge_location['days'] ) ) {
			echo( $charge_location['days'][0] );
		} ?>">

    </td>
    <td class="duration" width="5%">
        <input style="width: 100%" type="text" class="input_text cost" placeholder="cost"
               name="_rental_charge_locations[<?php echo esc_attr( $j ); ?>][cost][]"
               value="<?php
		       if ( isset( $charge_location['cost'] ) ) {
			       echo( $charge_location['cost'][0] );
		       } ?>">

    </td>
    <td width="1%"><a href="#" class="delete">x</a></td>


</tr>